import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { user } from '../model/user.interface';
import { JwtHelperService } from '@auth0/angular-jwt';

export interface loginInterface {
  email: string;
  password: string;
}

export function tokenGetter() {
  return localStorage.getItem("token");
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  token: string | null = localStorage.getItem('token')
  constructor(private http: HttpClient, private jwt:JwtHelperService) {
  }

  login(loginForm: loginInterface) {
    return this.http
      .post<{ access_token: string }>('/api/users/login', loginForm)
      .pipe(
        map((token) => {
          localStorage.setItem('token', token.access_token);
          return token;
        })
      );
  }
  register(registerForm: user) {
    return this.http
      .post<user>('api/users/', registerForm)
      .pipe(
        map((user) => user),
        catchError((error) => {
          return throwError(() => error);
        })
      );
  }
  isExpired(): boolean {
    return !this.jwt.isTokenExpired(this.token);
  }
  getUserId(): Observable<number>{
    return of(this.token).pipe(
      switchMap((jwt: any) => of(this.jwt.decodeToken(jwt)).pipe(
        map((jwt: any) => jwt.user.id)
      )
    ));
  }

}
