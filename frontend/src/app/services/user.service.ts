import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { user } from '../model/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  token: string | null;
  users: user[];

  constructor(private http: HttpClient) {}

  findAllUsers(): Observable<user[]> {
    return this.http.get<user[]>('api/users');
  }

  findByUserName(userName: string): Observable<user[]> {
    const params = new HttpParams().set('userName', userName);
    const options = { params: params };
    return this.http.get<user[]>('api/users', options);
  }

  findOne(userId: number): Observable<user> {
    return this.http.get<user>(`api/users/${userId}`);
  }

  updateOne(user: user): Observable<user> {
    return this.http.put<user>(`api/users/${user.id}`, user);
  }
}
