import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, catchError, map, takeUntil, throwError } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

class customRegisterValidator {
  static strongPasswordMatcher(
    control: AbstractControl
  ): ValidationErrors | null {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('confirmPassword')?.value;

    if (password && password !== confirmPassword) {
      return { passwordMismatch: true };
    }
    return null;
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy{
  registerForm: FormGroup;
  registerError: string[];
  
  private subject$ = new Subject()

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.registerForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
        username: ['', Validators.required],
        name: ['', Validators.required],
      },
      { validator: customRegisterValidator.strongPasswordMatcher } as AbstractControlOptions
    );
  }
  ngOnInit(): void {
    console.log(this.registerError)
    this.registerForm.valueChanges.pipe(takeUntil(this.subject$)).subscribe(()=> {
      this.registerError = []; 
      if(this.registerForm.errors){
        this.registerError.push(this.registerForm.errors['passwordMismatch'] ? 'Password mismatch' : '')
      } 
    })
  }

  onRegister() {
    if (!this.registerForm.valid) {
      return;
    }
    this.authService
      .register(this.registerForm.value)
      .pipe(
        map(() => {
          this.router.navigate(['login']);
        }),
        catchError((error) => {
          this.registerError.push(error.error.message);
          return throwError(() => error);
        })
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.subject$.next(null);
    this.subject$.complete();
  }
}
