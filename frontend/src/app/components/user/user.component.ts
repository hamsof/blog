import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { InputText } from 'primeng/inputtext';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { user } from 'src/app/model/user.interface';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {
  users: user[] = [];
  userInfo: string;
  token: string | null;
  headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    private UserService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.token = localStorage.getItem('token');
    this.headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`,
    });
  }

  ngOnInit(): void {
    this.initDataSource();
  }

  initDataSource() {
    this.UserService.findAllUsers()
      .pipe(
        map((users: user[]) => {
          this.users = users;
        })
      )
      .subscribe();
  }

  onUserInfo(userName: string) {
    this.UserService.findByUserName(userName)
      .pipe(
        map((users: user[]) => {
          this.users = users;
        })
      )
      .subscribe();
  }
  onUserClick(id: number) {
    this.router.navigate(['./' + id], { relativeTo: this.activatedRoute });
  }
}
