import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss'],
  providers: [MessageService],
})
export class UpdateProfileComponent {
  form: FormGroup;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {
    this.form = this.fb.group({
      id: [{ value: null, disabled: true }, [Validators.required]],
      name: [null, [Validators.required]],
      username: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.authService.getUserId().subscribe((userId: number) => {
      this.userService.findOne(userId).subscribe((user) => {
        this.form.patchValue(user);
      });
    });
  }

  update(): void {
    this.userService.updateOne(this.form.getRawValue()).subscribe((user) => {
      this.messageService.add({
        severity: 'warn',
        summary: 'Yesssss',
        detail: 'Profile Updated',
      });
    });
  }
}
