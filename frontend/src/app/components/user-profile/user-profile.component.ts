import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { Observable, map, Subject, takeUntil, tap, switchMap } from 'rxjs';
import { user } from 'src/app/model/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent {
  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {}
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private userId: number;
  user: user;
  ngOnInit() {
    this.route.params
      .pipe(
        takeUntil(this.destroy$),
        switchMap((params: Params) => {
          return this.userService.findOne(+params['id']);
        }),
        map((user: user) => {
          this.user = user;
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
