import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  items!: MenuItem[];
  
  constructor(){}

  ngOnInit() {
    this.items = [
      {
        label: 'Search',
        icon: 'pi pi-fw pi-search',
        items:[
          {
            label: 'Update Profile',
            icon: 'pi pi-fw pi-home',
            routerLink: 'update-profile'
          },
          {
            label: 'Login',
            icon: 'pi pi-fw pi-info-circle',
            routerLink: 'login'
          },
          {
            label: 'Register',
            icon: 'pi pi-fw pi-envelope',
            routerLink: 'register'
          },
        ]
      },  
      {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        routerLink: 'home'
      },
      {
        label: 'User',
        icon: 'pi pi-fw pi-envelope',
        routerLink: 'users'
      },
      { 
        label: 'Admin',
        icon: 'pi pi-fw pi-info-circle',
        routerLink: 'admin',
      },
    ];
  }
}
