export interface user {
  name: string;
  email: string;
  username: string;
  password?: string;
  id: number;
  profile: string;
  role: string;
}
