import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverviewComponent } from './components/overview/overview.component';
import { AppRoutingModule } from './admin-routing.module';



@NgModule({
  declarations: [
    OverviewComponent
  ],
  imports: [
    CommonModule, AppRoutingModule
  ]
})
export class AdminModule { }
