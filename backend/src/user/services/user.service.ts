import {
  ConflictException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../model/user.entity';
import { ILike, Repository } from 'typeorm';
import { Role, User } from '../model/user.dto';
import {
  Observable,
  catchError,
  from,
  map,
  switchMap,
  tap,
  throwError,
} from 'rxjs';
import { AuthService } from 'src/auth/service/auth.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private authService: AuthService,
  ) {}

  create(user: User): Observable<UserEntity> {
    return from(
      this.userRepository.findOne({ where: { email: user.email } }),
    ).pipe(
      switchMap((existingUserByEmail) => {
        if (existingUserByEmail) {
          return throwError(
            () => new ConflictException('Email already exists'),
          );
        }
        return from(
          this.userRepository.findOne({ where: { username: user.username } }),
        );
      }),
      switchMap((existingUserByUsername) => {
        if (existingUserByUsername) {
          return throwError(
            () => new ConflictException('Username already exists'),
          );
        }
        return this.authService.hashPassword(user.password);
      }),
      switchMap((hashedPassword) => {
        user.password = hashedPassword;
        user.role = Role.USER;
        return from(this.userRepository.save(user));
      }),
      map((savedUser: any) => {
        const { password, ...userWithoutPassword } = savedUser;
        return userWithoutPassword;
      }),
      catchError((error) => {
        return throwError(() => error);
      }),
    );
  }

  findAll(): Observable<User[]> {
    return from(this.userRepository.find()).pipe(
      map((users) => {
        users.forEach((user) => {
          delete user.password;
        });
        return users;
      }),
    );
  }

  findByUserName(username: string): Observable<User[] | null> {
    return from(
      this.userRepository.find({
        where: {
          username: ILike(`%${username}%`),
        },
      }),
    ).pipe(
      map((users) => {
        if (users.length > 0) {
          return users;
        } else {
          return null;
        }
      }),
    );
  }

  findOne(id: number): Observable<User> {
    return from(this.userRepository.findOne({ where: { id: id } }));
  }

  delete(id: number): Observable<any> {
    return from(this.userRepository.delete(id));
  }

  update(id: number, user: User): Observable<User> {
    delete user.email;
    delete user.password;
    delete user.role;
    return from(this.userRepository.update(id, user)).pipe(
      switchMap(() => this.findOne(id)),
    );
  }

  findByEmail(email: string): Observable<User> {
    return from(this.userRepository.findOne({ where: { email: email } }));
  }

  login(user: User): Observable<any> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: User) => {
        return this.authService.generateJWT(user).pipe(
          map((jwt: string) => {
            return { access_token: jwt };
          }),
        );
      }),
    );
  }

  validateUser(email: string, password: string): Observable<User> {
    return from(
      this.userRepository.findOne({
        where: { email },
        select: [
          'id',
          'password',
          'name',
          'username',
          'email',
          'role',
          'profile',
        ],
      }),
    ).pipe(
      switchMap((user: User) => {
        if (!user) {
          throw new HttpException('Email Not found', HttpStatus.NOT_FOUND);
        }
        return this.authService.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              const { password, ...result } = user;
              return result;
            } else {
              throw new UnauthorizedException('Invalid password');
            }
          }),
        );
      }),
    );
  }

  updateUserRole(id: number, role: Role): Observable<User> {
    return from(this.userRepository.findOne({ where: { id: id } })).pipe(
      map((user) => {
        if (user) {
          user.role = role;
          this.userRepository.save(user);
          return user;
        } else {
          throw new NotFoundException('User not found');
        }
      }),
    );
  }
}
