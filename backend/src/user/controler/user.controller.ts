import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  Request,
  StreamableFile,
  Res,
  Header,
  NotFoundException,
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { Role, User } from '../model/user.dto';
import { Roles } from 'src/auth/decorator/role.decorator';
import { RolesGuard } from 'src/auth/gaurd/role.gaurd';
import { AuthGuard } from 'src/auth/gaurd/auth.gaurd';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Observable, map, of } from 'rxjs';
import { createReadStream, existsSync } from 'fs';
import { join } from 'path';
import { UserGaurd } from 'src/auth/gaurd/user.gaurd';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Roles(Role.USER)
  @UseGuards(AuthGuard, RolesGuard)
  @Get()
  findAll(@Query('userName') userName: string) {
    if (userName) {
      return this.userService.findByUserName(userName);
    }
    return this.userService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @Post()
  create(@Body() user: User) {
    return this.userService.create(user);
  }

  @UseGuards(AuthGuard, UserGaurd)
  @Put(':id')
  update(@Param('id') id: string, @Body() user: User) {
    return this.userService.update(+id, user);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.userService.delete(+id);
  }

  @Post('login')
  login(@Body() user: User) {
    return this.userService.login(user);
  }

  @Roles(Role.ADMIN)
  @UseGuards(AuthGuard, RolesGuard)
  @Put('role/:id/:role')
  role(@Param('id') id: string, @Param('role') role: Role) {
    return this.userService.updateUserRole(+id, role);
  }

  @UseGuards(AuthGuard)
  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads/profileImages',
        filename: (req, file, cb) => {
          const filename: string = file.originalname;
          const extension: string = filename.split('.')[1];
          const randomName = Array(4)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          cb(null, `${randomName}.${extension}`);
        },
      }),
    }),
  )
  uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Request() req,
  ): Observable<Object> {
    return this.userService
      .update(req.user.id, {
        profile: file.filename,
      })
      .pipe(
        map((user) => ({
          profile: user.profile,
        })),
      );
  }

  @Get('profile-image/:imageName')
  @Header('Content-Type', 'image/png')
  @Header('Content-Disposition', 'attachment; filename="file.png"')
  getStaticFile(@Param('imageName') imageName: string): StreamableFile {
    const file = join(process.cwd(), 'uploads/profileImages', imageName);
    if (!existsSync(file)) {
      throw new NotFoundException('File not found');
    }
    return new StreamableFile(createReadStream(file));
  }
}
