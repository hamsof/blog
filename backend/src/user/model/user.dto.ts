import { BlogEntry } from "src/blog/model/blog.interface";

export interface User {
    id?: number;
    name?: string;
    email?: string;
    username?: string;
    password?: string;
    role?: Role;
    profile?: string;
    blogEntries?: BlogEntry[];
}

export enum Role{
    ADMIN = 'admin',
    OWNER = 'owner',
    USER = 'user'
}