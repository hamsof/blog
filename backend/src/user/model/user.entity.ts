import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./user.dto";
import { BlogEntity } from "src/blog/model/blog.entity";

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ unique: true })
    email: string;

    @Column({ unique: true })
    username: string;

    @Column({select: false})
    password: string;

    @Column({type: 'enum', enum:Role, default:Role.USER})
    role: Role;

    @Column({nullable: true})
    profile: string;

    @BeforeInsert()
    emailToLowerCase() {
        this.email = this.email.toLowerCase();
    }

    @OneToMany(type=> BlogEntity, blog=> blog.author)
    blogEntries: BlogEntity[]
}