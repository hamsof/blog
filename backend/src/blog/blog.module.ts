import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogEntity } from './model/blog.entity';
import { AuthModule } from 'src/auth/auth.module';
import { UserModule } from 'src/user/user.module';
import { BlogController } from './controllers/blog/blog.controller';
import { BlogService } from './services/blog/blog.service';
import { JwtModule } from '@nestjs/jwt';
import { UserService } from 'src/user/services/user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([BlogEntity]),
    JwtModule,
    AuthModule,
    UserModule,
  ],
  providers: [BlogService],
  controllers: [BlogController],
})
export class BlogModule {}
