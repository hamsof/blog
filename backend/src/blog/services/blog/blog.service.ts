import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Observable, from, map, of, switchMap, tap, throwError } from 'rxjs';
import { BlogEntity } from 'src/blog/model/blog.entity';
import { BlogEntry } from 'src/blog/model/blog.interface';
import { User } from 'src/user/model/user.dto';
import { Repository } from 'typeorm';
let slugify = require('slugify');
@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(BlogEntity)
    private readonly blogRepository: Repository<BlogEntity>,
  ) {}

  post(blog: BlogEntry, user: User): Observable<BlogEntry> {
    blog.author = user;
    return this.findByTitle(blog.title).pipe(
      switchMap((isBlogExists) => {
        if (isBlogExists) {
          return throwError(() => new ConflictException('blog title exists'));
        } else {
          return this.slug(blog.title);
        }
      }),
      switchMap((s: string) => {
        blog.slug = s;
        return from(this.blogRepository.save(blog));
      }),
      map((blog: BlogEntry) => {
        //can be custom response of
        return blog;
      }),
    );
  }

  slug(title: string): Observable<string> {
    return of(slugify(title));
  }

  findOne(id: number): Observable<BlogEntry> {
    return from(this.blogRepository.findOne({ where: { id: id } }));
  }

  findByTitle(title: string): Observable<BlogEntity> {
    return from(this.blogRepository.findOne({ where: { title: title } }));
  }

  findAll(): Observable<BlogEntry[]> {
    return from(this.blogRepository.find());
  }

  findByUserId(userId: number): Observable<BlogEntry[]> {
    return from(
      this.blogRepository.find({
        relations: {
          author: true,
        },
        where: {
          author: { id: userId },
        },
      }),
    );
  }
}
