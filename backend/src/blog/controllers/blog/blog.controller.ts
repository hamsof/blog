import {
  Body,
  Controller,
  Post,
  UseGuards,
  Request,
  Query,
  Get,
  Param,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthGuard } from 'src/auth/gaurd/auth.gaurd';
import { UserGaurd } from 'src/auth/gaurd/user.gaurd';
import { BlogEntry } from 'src/blog/model/blog.interface';
import { BlogService } from 'src/blog/services/blog/blog.service';

@Controller('blog-entries')
export class BlogController {
  constructor(private blogService: BlogService) {}

  @UseGuards(AuthGuard)
  @Post()
  create(@Body() blog: BlogEntry, @Request() req): Observable<BlogEntry> {
    const user = req.user;
    return this.blogService.post(blog, user);
  }

  @Get('user/:userId')
  findByUser(@Param('userId') userId: string): Observable<BlogEntry[]> {
    return this.blogService.findByUserId(+userId);
  }

  @Get()
  findAll(): Observable<BlogEntry[]> {
    return this.blogService.findAll();
  }
}
