import { CanActivate, ExecutionContext, Inject, Injectable, forwardRef } from '@nestjs/common';
import { Observable, map } from 'rxjs';
import { User } from 'src/user/model/user.dto';
import { UserService } from 'src/user/services/user.service';



Injectable()
export class UserGaurd implements CanActivate{
    constructor(
        @Inject(forwardRef(() => UserService))
        private userService: UserService
    ) {

    }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        debugger;
        const request = context.switchToHttp().getRequest()
        const user: User  = request.user;
        const id = +request.params.id
        return this.userService.findOne(user.id).pipe(
            map((user: User)=> {
                if(user.id=== id){
                    return true;
                }
                return false;
            })
        )
    }
}