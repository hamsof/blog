import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable, from } from 'rxjs';
import { User } from 'src/user/model/user.dto';

@Injectable()
export class AuthService {
  bcrypt = require('bcrypt');
  constructor(private jwtService: JwtService) {}

  generateJWT({ id, email, role }: User): Observable<string> {
    return from(this.jwtService.signAsync({ user: { id, email, role } }));
  }

  hashPassword(password: string): Observable<string> {
    return from<string>(this.bcrypt.hash(password, 12));
  }

  comparePasswords(
    newPassword: string,
    passwordHash: string,
  ): Observable<any | boolean> {
    return from<any | boolean>(this.bcrypt.compare(newPassword, passwordHash));
  }
}
