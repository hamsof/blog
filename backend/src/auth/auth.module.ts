import { Module } from '@nestjs/common';
import { AuthController } from './contoller/auth.controller';
import { AuthService } from './service/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthGuard } from './gaurd/auth.gaurd';
import { RolesGuard } from './gaurd/role.gaurd';

@Module({
    imports: [
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: (configService: ConfigService) => ({
                secret: configService.get<string>('JWT_SECRET'), 
                signOptions: { expiresIn: '60000s' },
            }),
            inject: [ConfigService], 
        }),

    ],
    controllers: [AuthController],
    providers: [AuthService, AuthGuard, RolesGuard],
    exports: [AuthService],
})
export class AuthModule { }
