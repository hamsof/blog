FRONTEND_COMMAND = cd frontend && ng serve
BACKEND_COMMAND = cd backend && npm run start:dev
start:
	$(FRONTEND_COMMAND) & $(BACKEND_COMMAND)